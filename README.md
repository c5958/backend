# backend



## Getting started

This is backend service used for crawler API.

Build it using `docker-compose build backend`.\
Start it using `docker-compose up backend`.\
Run tests `docker-compose exec ./docker/run-tests.sh`

This service should be placed inside dev-env.

It should be automatically cloned by running `./setup.sh` in dev-env repo